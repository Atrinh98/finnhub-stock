# Finnhub-Stock

## Demo

https://www.youtube.com/watch?v=Prl1UiheHyk

## Members

- Andrew Trinh
- Shiv Patel

## Description

A simple website where you get the price of stocks.

## Technology Stack

- JavaScript
- Node
- Express
- HTML
- CSS
- Finnhub API

## How to Run

### Run locally

Open the Finnhub-Stock project folder in your preferred IDE

1. Open the terminal in your chosen IDE and ensure you are in the root directory `.\finnhub-stock\`
2. Install the dependencies with: `npm install`
3. To start server and client run: `npm run start`

### Run Remote

Got to the following link: https://finnhub-stock-price.onrender.com/

## Usage

Once on the website either locally or remotely, you will be presented with the home page that has a text field.

You can:
- Start typing and it will show all the available stocks.
- Select the stock and press Go! to see the price.
    - If everything goes correctly, the stock price will show up.
    - Else, an error message appears.

## Credits

https://finnhub.io/\
https://www.pexels.com/photo/stock-exchange-board-210607/