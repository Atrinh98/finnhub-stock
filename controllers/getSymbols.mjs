/** 
 * @author Shiv Patel and Andrew Trinh
 * @date 2022, October 28
*/

// Imports
import fetch from 'node-fetch'
import dotenv from 'dotenv';
dotenv.config();

// Url datas
let url = new URL("https://finnhub.io/api/v1/quote?");
url.searchParams.append("token", process.env.TOKEN);

/**
 * Fetches the cost result with the provided stock symbol.
 * @param {string} symbol. the stock symbol.
 * @returns the cost of the symbol.
 */
export default async function getCost(symbol){
  url.searchParams.set("symbol", symbol);
  try{
    let data = await fetch(url);
    data = await data.json();
    let cost = data.c;
    return cost;
  } catch (err){
    console.error(err);
    throw err;
  }
}