/** 
 * @author Shiv Patel (1935098) and Andrew Trinh (1537132)
 * @date 2022, October 28
*/

// Imports
import readApi from '../fileio/fileReader.mjs';

/**
 * Class for symbols
 */
export default class Symbol{

  /**
   * Constructor for symbol class.
   * @returns the instance of symbol class.
   */
  constructor(){
    if (!Symbol._instance){
      Symbol._instance = this;
    } else{
      return Symbol._instance;
    }
  }

  /**
   * Reads the filepath and returns the data (json symbols)
   * @param {string} path, the filepath to get the data (json symbols) 
   * @returns returns an array of symbols (json)
   */
  async readFile(path){
    try{
      return await readApi(path);
    } catch (err){
      console.error(err);
      throw err;
    }
  }
}