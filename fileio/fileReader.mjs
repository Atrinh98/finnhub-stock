/** 
 * @author Shiv Patel and Andrew Trinh
 * @date 2022, October 28
*/

// Imports
import * as fs from 'node:fs/promises';

/**
 * Reads the path and returns the data (json symbols)
 * @param {string} path, the path to get the data (json symbols)
 * @returns json file with all the symbols.
 */
export default async function readApi(path){
  try{
    const data = await fs.readFile(path, 'utf8');
    return data;
  } catch (err){
    console.error(err);
    throw err;
  }
}