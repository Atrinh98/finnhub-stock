/** 
 * @author Shiv Patel and Andrew Trinh
 * @date 2022, October 28
*/

import Symbol from './controllers/symbols.mjs'; 
import getCost from './controllers/getSymbols.mjs'; 
import express from 'express';

const app = express();
const port = 3000;
let dataStock;

/**
 * IIFE to get the list of symbols from reading a path to the basicNasdaq.json file.
 */
(async () => {
  let symbol = new Symbol();
  try{
    let data = await symbol.readFile("./files/basicNasdaq.json");
    dataStock = JSON.parse(data);
  } catch (err){
    console.error(`File not found / Path not good`);
  }
})();

/**
 * Route to get the datalist (all stock symbols) as a json.
 */
app.get('/datalist', (req, res) =>{
  res.json(dataStock);
})

/**
 * Route to get the cost of the symbol.
 */
app.get('/cost', async (req, res) =>{
  let symbol = req.query.symbol;
  try{
    let cost = await getCost(symbol);
    res.json(cost);
  } catch (err){
    console.error(`404! Symbol not found`);
  }
})

/**
 * Route to use the static public folder.
 */
app.use(express.static('public'));

/**
 * If the user gets to this route, send a 404 message.
 */
app.use(function (req, res) {
  res.status(404).send("<h1>404!! The page you are trying to access does not exist</h1>");
});

/**
 * When the server starts.
 */
app.listen(port, () => {
  console.log(`Listening on ${port}, Server started!!!`)
});