/** 
 * @author Shiv Patel and Andrew Trinh
 * @date 2022, October 28
*/

const port = 3000;
const baseURL = `http://localhost:${port}/`;


// Global variables
window.addEventListener('DOMContentLoaded', init);
const datalist = document.querySelector("#stockList");
let symbols = [];

/**
 * Initial setup when the client loads the browser.
 * Puts an event to the click button.
 * Fetches the data.
 */
function init(){
  let submit = document.querySelector("#submit");
  submit.addEventListener("click", getSymbol);
  getData();
}

/**
 * Fetch the datalist from the server and then send it to the populate() function.
 */
async function getData(){
  let symbolURL = new URL(`${baseURL}datalist`);
  try{
    let resp = await fetch(symbolURL);
    if (resp.ok){
      let json = await resp.json();
      populate(json);
    }else{
      throw new Error('Request Failed'); 
    }
  } catch (err){
    console.error(`Could not fetch Datalist`);
  }
}

/**
 * When the user clicks, the function gets the value from the search input and validates.
 * If the symbol is not from the list of symbols, display an error.
 * If the symbol is good, fetch from the server the cost and display the symbol with it's cost.
 */
async function getSymbol(e){
  e.preventDefault();
  const form = document.querySelector('#myForm');
  let formData = new FormData(form);
  let symbol = formData.get('symbol');
  if (!symbols.includes(symbol)){
    displayError(symbol);
    console.error(`The provided symbol: ${symbol}, is an invalid one`);
  } else {
    let url = new URL(`${baseURL}cost?symbol=${symbol}`);
    let resp = await fetch(url);
    if (resp.ok){
      let cost = await resp.json();
      displayResult(symbol, cost);
    }   else{
      console.error(`Failed to fetch ${symbol}`);
    }
  }
}

/**
 * Changes an element in the DOM to display an error message about an invalid symbol
 * @param {string} symbol, the symbol associated to the stock name.
 */
function displayError(symbol){
  let result = document.querySelector("#result");
  result.removeAttribute('class');
  if(symbol.length === 0){
    result.textContent = "Symbol not inputted!!!"
  } else{
    result.textContent = `The provided symbol is invalid: ${symbol}`;
  }
  result.classList.add("error");
}

/**
 * Validation to make sure that the cost is a number or not equal to 0.
 * Then displays the result on the DOM.
 * @param {*} symbol, the symbol associated to the stock name.
 * @param {number} cost, the cost of the symbol.
 */
function displayResult(symbol, cost){
  let result = document.querySelector("#result");
  result.removeAttribute('class');
  if (!isNaN(cost) || cost === 0){
    result.textContent = `Current price of ${symbol} is: ${cost}$`;
    result.classList.add("display");
  } else{
    result.textContent = `Symbol not found`;
    result.classList.add("error");
  }
}
  
/**
 * Creates and appends an option element to the datalist for each of the symbols in the data param
 * @param {json} data, list of all symbols.
 */
function populate(data){
  for(let i = 0; i < data.length; i++){
    symbols.push(data[i].symbol);
    let opt = document.createElement('option');
    opt.value = symbols[i];
    datalist.appendChild(opt);
  }
}